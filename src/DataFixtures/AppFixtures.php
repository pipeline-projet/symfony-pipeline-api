<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        for($u=0; $u <10; $u++){

            $article=new Article();
            
            $article->setName('Article n '.$u);

            $article->setContent('je suis un contenu de qualité n'.$u);

            $manager->persist($article);
        }
        $manager->flush();
    }
    
}

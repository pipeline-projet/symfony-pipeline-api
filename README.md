# Symfony API CI/CD

Ce projet symfony réalisé par Yoann Singer, Alexandre Mazeau et Enzo Fonteneau consiste à pouvoir appeler grâce à une API des articles dans la base de données tout en utilisant un environnement docker.

## Installation

1. Clonez ce projet dans un dossier
2. Installez Docker si ce n'est pas fait
3. À l'intérieur de ce dossier, tapez ces commandes :

```shell
docker-compose build
docker-compose up -d
docker-compose exec php bash
```

5. Une fois à l'intérieur du conteneur php, tapez les commandes suivantes :
```shell
composer install
composer require symfony/apache-pack
php bin/console doctrine:database:create --if-not-exists
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load       
```

## Demo

6. À partir d'ici vous pouvez aller sur votre navigateur préféré et taper dans l'url afin de consulter tous les articles existants sur le site :
```
localhost:8080/api/articles
```

   Vous pouvez aussi taper l'url suivante en remplaçant "{id}" par un l'id d'un des articles existants :
```
localhost:8080/api/articles/{id}
```

## Informations supplémentaires

Vous pouvez consulter le diaporama de présentation de ce projet à cette adresse :
```
https://docs.google.com/presentation/d/19fiJiu0ng16ZkLzfbvF9bUVKCHNIXEkrC2eI4psNUWs/edit?usp=sharing
```


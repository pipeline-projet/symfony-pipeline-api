<?php
//api/tests/ArticlesTest.php

use App\Entity\Article;
namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class ArticlesTest extends ApiTestCase
{
    public function testGetCollection(): void
    {
        static::createClient()->request('GET', 'localhost:8080/api/articles');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        
        //Récupération de l'article ayant l'ID correspondant à 1
        $article1 = static::createClient()->request('GET', 'localhost:8080/api/articles/1')->getContent();

        //Vérification de la récupération précédente
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        //Vérification si le contenu récupéré est bien du JSON
        $this->assertJsonEquals($article1);

        //Transformation du JSON en objet
        $article1 = json_decode($article1);
        
        //Vérification des valeurs de l'objet
        $this->assertTrue($article1->id == "1");
        $this->assertTrue($article1->name == "Article n 0");
        $this->assertTrue($article1->content == "je suis un contenu de qualité n0");
    }
}